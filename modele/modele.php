<?php

////////////////////////////////////////////////////////////////////////
///////    Gestion de la connxeion   ///////////////////////////////////
////////////////////////////////////////////////////////////////////////

/**
 * Initialise la connexion à la base de données courante (spécifiée selon constante 
 *	globale SERVEUR, UTILISATEUR, MOTDEPASSE, BDD)			
 */
function open_connection_DB()
{
	global $connexion;

	$connexion = mysqli_connect(SERVEUR, UTILISATEUR, MOTDEPASSE, BDD);
	if (mysqli_connect_errno()) {
		printf("Échec de la connexion : %s\n", mysqli_connect_error());
		exit();
	}
}

/**
 *  	Ferme la connexion courante
 * */
function close_connection_DB()
{
	global $connexion;

	mysqli_close($connexion);
}


////////////////////////////////////////////////////////////////////////
///////   Accès au dictionnaire       ///////////////////////////////////
////////////////////////////////////////////////////////////////////////


/**
 *  Retourne la liste des tables définies dans la base de données courantes (BDD)
 * */
function get_tables()
{
	global $connexion;

	$requete = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA LIKE '" . BDD . "'";

	$res = mysqli_query($connexion, $requete);
	$instances = mysqli_fetch_all($res, MYSQLI_ASSOC);
	return $instances;
}



////////////////////////////////////////////////////////////////////////
///////    Informations (structure et contenu) d'une table    //////////
////////////////////////////////////////////////////////////////////////




/**
 * Retourne le résultat (schéma et instances) de la requ$ete $requete
 * */
function executer_une_requete($requete)
{

	global $connexion;
	$res = mysqli_query($connexion, $requete);
	if (!$res)
		return null;


	else {
		$info_att = mysqli_fetch_fields($res);
		$schema = array();
		foreach ($info_att as $att)
			array_push($schema, array('nom' => $att->{'name'}));

		$instances = mysqli_fetch_all($res, MYSQLI_ASSOC);
	}

	return array('schema' => $schema, 'instances' => $instances);
}





function stats()
{

	$stat = array();

	array_push($stat, executer_une_requete("
	SELECT
    (SELECT COUNT(*) FROM joueur) AS nombre_joueurs,
    (SELECT COUNT(*) FROM equipe) AS nombre_equipes,
    (SELECT COUNT(*) FROM classement) AS nombre_classements,
    (SELECT COUNT(*) FROM tournoi) AS nombre_tournois,
    AVG(nbJoueur) AS moyenne_participants_par_tournoi
FROM partie;


    "));

	array_push($stat, executer_une_requete("
	SELECT tournoi.nom, YEAR(tournoi.dateDeb) AS annee_tournoi, phase.niveau
	FROM phase
	INNER JOIN tournoi ON phase.idT = tournoi.idT
	INNER JOIN participe ON phase.idT = participe.idT AND phase.niveau = participe.niveau
	WHERE participe.idJ = 1
	ORDER BY annee_tournoi DESC, phase.niveau DESC;
	
    "));

	array_push($stat, executer_une_requete("
	SELECT COUNT(*) AS nbeq
	FROM classementEquipe ce
	WHERE ce.idCla IN (
		SELECT c1.idCla
		FROM classement c1
		LEFT JOIN classementIndividuel ci ON c1.idCla = ci.idCla
		WHERE ci.idCla IS NULL
	);
	
    "));

	array_push($stat, executer_une_requete("

    SELECT YEAR(dateDeb) AS annee_tournoi, AVG(nbJoueur) AS moyennep
	FROM tournoi
	INNER JOIN partie ON tournoi.idT = partie.idPa
	WHERE YEAR(dateDeb) >= YEAR(CURDATE()) - 3
	GROUP BY YEAR(dateDeb)
	ORDER BY YEAR(dateDeb) DESC;

    "));

	array_push($stat, executer_une_requete("
    SELECT j.nom, j.prenom
	FROM joueur j
	WHERE j.idJ IN (
	SELECT ec.idJ
	FROM estClasse ec
	INNER JOIN classementIndividuel ci ON ec.idCla = ci.idCla
	WHERE ci.portee = 'nationale'
	GROUP BY ec.idJ
	HAVING COUNT(DISTINCT ec.idCla) >= 2
	)
	ORDER BY j.nom, j.prenom;

    "));

	array_push($stat, executer_une_requete("
	SELECT taille, COUNT(*) AS nombre_parties
	FROM plateau
	GROUP BY taille;
	
    "));

	array_push($stat, executer_une_requete("
	SELECT j.pseudo, COUNT(*) AS nombre_parties_jouees
	FROM joueur j
	INNER JOIN joue ON j.idJ = joue.idJ
	GROUP BY j.idJ
	ORDER BY nombre_parties_jouees DESC
	LIMIT 5;
	
    "));

	return $stat;
}


function toutePartie()
{
	$partie = array();

	array_push($partie, executer_une_requete("
	SELECT idPa
	FROM partie;
    "));
	return $partie;
}

function toutePartie_recente()
{
	$partie = array();

	array_push($partie, executer_une_requete("
	SELECT idPa
	FROM partie
	ORDER BY dateP DESC, horaire DESC
	LIMIT 50;

    "));
	return $partie;
}

function toutePartie_rapide()
{
	$partie = array();

	array_push($partie, executer_une_requete("
	SELECT idPa
	FROM partie
	ORDER BY nbJoueur DESC, duree ASC
	LIMIT 50;





    "));
	return $partie;
}

function infoPartie($id_partie)
{
	$partie = array();

	array_push($partie, executer_une_requete("
	SELECT dateP, horaire
	FROM partie
	WHERE idPa = $id_partie;

	
    "));

	array_push($partie, executer_une_requete("
	SELECT taille
	FROM plateau
	WHERE idPa = $id_partie;

	
    "));

	array_push($partie, executer_une_requete("
	SELECT
    (SELECT COUNT(*) FROM possede p INNER JOIN carte c ON p.idCa = c.idCa WHERE c.niveau = 'vert' AND p.idPa = $id_partie) AS nbB,
    (SELECT COUNT(*) FROM possede p INNER JOIN carte c ON p.idCa = c.idCa WHERE c.niveau = 'orange' AND p.idPa = $id_partie) AS nbJ,
    (SELECT COUNT(*) FROM possede p INNER JOIN carte c ON p.idCa = c.idCa WHERE c.niveau = 'noir' AND p.idPa = $id_partie) AS nbR;



  "));

	array_push($partie, executer_une_requete("
	SELECT j.pseudo
	FROM joue jo
	INNER JOIN joueur j ON jo.idJ = j.idJ
	WHERE jo.idPa = $id_partie;

	
    "));

	array_push($partie, executer_une_requete("
	SELECT joueur.pseudo, joue.score
	FROM joue
	INNER JOIN joueur ON joue.idJ = joueur.idJ
	WHERE joue.idPa = $id_partie
	ORDER BY joue.score DESC
	LIMIT 3;

	
    "));



	return $partie;
}





function creePartie($mode, $nbJoueur, $taille, $nbverte, $nborange, $nbnoire)
{

	$date = date('Y-m-d');
	$heure = date('H:i:s');

	executer_une_requete("INSERT INTO partie (dateP, horaire, duree, etat, modeJeu, nbJoueur)
			VALUES ('$date', '$heure', 'A venir', $mode, $nbJoueur);");



	$idP =  executer_une_requete("SELECT LAST_INSERT_ID() as id;")['instances'][0]['id'];

	executer_une_requete("INSERT INTO plateau (idPa, taille)
			VALUES ($idP, $taille);");


	executer_une_requete("INSERT INTO estCompose (idPa, taille, idCa)
			SELECT $idP, [taille_du_plateau], idCa
			FROM carte
			WHERE niveau = 'vert'
			ORDER BY RAND()
			LIMIT $nbverte;");


	executer_une_requete("INSERT INTO estCompose (idPa, taille, idCa)
			SELECT $idP, [taille_du_plateau], idCa
			FROM carte
			WHERE niveau = 'orange'
			ORDER BY RAND()
			LIMIT $nborange;");


	executer_une_requete("INSERT INTO estCompose (idPa, taille, idCa)
			SELECT $idP, [taille_du_plateau], idCa
			FROM carte
			WHERE niveau = 'noir'
			ORDER BY RAND()
			LIMIT $nbnoire;");

	return $idP;

}




function toutJoueur(){



	return executer_une_requete("SELECT idJ FROM joueur;");
	
}




function ajouterJoueurPartie($idJ, $idP){
	executer_une_requete("INSERT INTO joue (idJ, idPa) VALUES ($idJ, $idP);");

}


function supprJoueurPartie($idJ, $idP){
	executer_une_requete("DELETE FROM joue WHERE idJ = $idJ AND idPa = $idP;);");
}	
















?>



