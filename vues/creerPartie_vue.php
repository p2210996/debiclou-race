<main>

<?php if (!isset($_POST['nbcouleur'])) {?>
<h2>Créer une nouvelle partie</h2>
    <form action="index.php?page=creerPartie" method="POST">
        <label for="nbcartes">Nombre de cartes constituant le plateau:</label><br>
        <input type="number" id="nbcartes" name="nbcartes" required><br><br>
        
        <label for="nbverte">Nombre de cartes vertes à sélectionner:</label><br>
        <input type="number" id="nbverte" name="nbverte" required><br><br>
        
        <label for="nborange">Nombre de cartes oranges à sélectionner:</label><br>
        <input type="number" id="nborange" name="nborange" required><br><br>
        
        <label for="nbnoire">Nombre de cartes noires à sélectionner:</label><br>
        <input type="number" id="nbnoire" name="nbnoire" required><br><br>
        
        

        <br><br>
        <label for="strategie">Stratégie d'ordre de passage:</label><br>
        <select id="strategie" name="strategie">
            <option value="+JEUNE">Honneur au plus jeune</option>
            <option value="-EXPE">Honneur au moins expérimenté</option>
            <option value="ALEA">Aléatoire</option>
        </select><br><br>




        <input type="submit" name="nbcouleur" value="Créer la partie">
    </form>
<?php  }?>




<?php  if (isset($_POST['nbcouleur'])){ ?>
    <h2>Sélection des joueurs</h2>
    <form action="index.php?page=creerPartie" method="POST">
        <label for="select_joueur">Sélectionner un joueur à ajouter à la partie:</label><br>
        <select id="select_joueur" name="ajo">
            
            <?php  foreach ($tab as $val) {
                echo "<option value='" . $val['idJ'] . "'>" . $val['idJ'] . "</option>";
                        }
            
            ?>
        </select><br><br>
        
        <input type="submit" name="aj" value="Ajouter le joueur">
        
        <br><br>
        <label for="remove_joueur">Sélectionner un joueur à retirer de la partie:</label><br>
        <select id="remove_joueur" name="suppri">
        <?php  foreach ($tab as $val) {
                echo "<option value='" . $val['idJ'] . "'>" . $val['idJ'] . "</option>";
                        }
            
            ?>
        </select><br><br>
        
        <input type="submit" name="suppr" value="Retirer le joueur">
        
        <br><br>
        
        <input type="submit" name="joueurs" value="Valider la partie">
    </form>


    <?php  } ?>

</main>