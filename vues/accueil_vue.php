<main>
	
	<div>
		<h2 id="auteur"> Description:  <br> </h2>
			<pre>
		Le principe du jeu est le suivant : chaque joueur est un cycliste qui doit arriver le premier au bout du
		parcours qui est composé de 12 cartes. Pour avancer sur une case, il faut que le joueur réalise la combinaison de
		dés demandée sur la carte. Pour cela, chaque joueur dispose de 6 dés de couleur bleue, 6 dés de couleur jaune
		et 6 dés de couleur rouge. À chaque tour, le joueur choisit 6 dés parmi les 18 dés de couleurs pour essayer de
		valider une ou plusieurs cases en au plus 3 lancers. Différentes stratégies peuvent être mises en place en fonction
		du nombre de cartes que le joueur souhaite valider en un tour.
			</pre> <br>
		

		<h2 id="auteur"> Statistiques:  <br> </h2>

		<table class="accueil">
		
			<tr>
				<td>Un n-uplet contenant le nombre de joueurs, le nombre d’équipes, <br>le nombre de classements, le nombre de
tournois et la moyenne des participants par tournoi</td>
				<td> <?php 
						
						echo "( " ;
						foreach ($stat[0]['instances'][0] as $val)
							echo $val . ", " ;
						echo  " )";
					?> 
				</td>
			</tr>
			<tr>
				<td>les phases de tournoi (nom et année de tournoi, niveau de phase) jouées et <br> pour lesquelles s’est qualifié l’utilisateur courant</td>
				<td>
				<?php 
					foreach ($stat[1]['instances'] as $key => $value)	{
						echo "( " ;
						foreach ($stat[1]['instances'][$key] as $val)
							echo $val . ", " ;
						echo  " )";
						} 
						echo '<br>';
						//print_r($stat[1]['schema']);
						
					?> 
				

				</td>
			</tr>
			<tr>
				<td>le nombre d’équipes classées premières des classements <br> dont aucun des membres n’est premier dansun classement individuel</td>
				<td><?php   
					echo $stat[2]['instances'][0]['nbeq'];
					?>
				</td>
			</tr>
			<tr>
				<td>Pour les 3 dernières années, le nombre moyen de participants <br>aux tournois</td>
				<td><?php   
					echo $stat[3]['instances'][0]['moyennep'];
					
					?>
				</td>
			</tr>
			<tr>
				<td>Le nom et le prénom des joueurs classés de manière individuelle dans le top 5 <br> d’au moins 2classements de portée nationale".</td>
				<td><?php  print_r($stat[4]) ?>  </td>
			</tr>
			<tr>
				<td>Le nombre de parties jouées avec un plateau <br>pour chaque taille de plateau,</td>
				<td><?php 
					foreach ($stat[5]['instances'] as $key => $value)	{
						echo "( " ;
						foreach ($stat[5]['instances'][$key] as $val)
							echo $val . ", " ;
						echo  " )";
						} 
						echo '<br>';
						
						
					?></td>
			</tr>
			<tr>
				<td>Le top 5 des joueurs (pseudo) qui ont joué <br>le plus de parties</td>
				<td><?php 
					foreach ($stat[6]['instances'] as $key => $value)	{
						
						foreach ($stat[6]['instances'][$key] as $val)
							echo $val. ", " ;
						
						} 
						
						
						
					?></td>
			</tr>
		</table>

	</div>

	


</main>






